﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T1_Diars_BlogPost.DB;
using T1_Diars_BlogPost.Models;

namespace T1_Diars_BlogPost.Controllers
{
    public class BlogController : Controller
    {
        private BlogPostContext context;
        public BlogController( BlogPostContext context)
        {
            this.context = context;
        }
        public ActionResult Index()
        {
            var blogs = context.Blogs.OrderByDescending(o =>o.fecha).ToList();
            return View(blogs);
        }

        [HttpGet]
        public ActionResult Detail(int id)
        {
            var blog = context.Blogs.Where(o=>o.id == id).First();
            var comentarios = context.Comentarios.Where(o => o.BlogId == id).ToList();
            var detalle = new DetalleBlog();
            detalle.blog = blog;
            detalle.ComentariosBlog = comentarios.OrderByDescending(o => o.fecha).ToList();
            return View("Detail", detalle);
        }

        [HttpPost]
        public ActionResult Detail(Comentario comentario)
        {           
            comentario.fecha = DateTime.Now;
            context.Comentarios.Add(comentario);
            context.SaveChanges();
            return RedirectToAction("Index");
        }





            [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Blog blog)
        {
            blog.fecha = DateTime.Now;
            context.Blogs.Add(blog);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        

    }
}
