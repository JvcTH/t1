﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace T1_Diars_BlogPost.Models
{
    public class DetalleBlog
    {       
        public Blog blog { get; set; }
        public List<Comentario> ComentariosBlog { get; set; }
    }
}
