﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace T1_Diars_BlogPost.Models
{
    public class Blog
    {
        public int id { get; set; }
        public string titulo { get; set; }
        public string autor { get; set; }
        public string contenido { get; set; }
        public DateTime fecha { get; set; }

      
    }
}
