﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace T1_Diars_BlogPost.Models
{
    public class Comentario
    {
        public int id { get; set; }
        public int BlogId { get; set; }
        public string contenido { get; set; }
        public DateTime fecha { get; set; }
    }
}
