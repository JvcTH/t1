﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T1_Diars_BlogPost.DB.Mapping;
using T1_Diars_BlogPost.Models;

namespace T1_Diars_BlogPost.DB
{
    public class BlogPostContext : DbContext
    {
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Comentario> Comentarios { get; set; }
        public BlogPostContext(DbContextOptions<BlogPostContext> options): base(options){}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new BlogMap());
            modelBuilder.ApplyConfiguration(new ComentarioMap());
        }
    }
}
